# HAL_STM32_FreeModbusV1.6_Demo

#### 介绍
STM32基于HAL库的移植FreeModbus示例工程

移植教程见博客地址：https://blog.csdn.net/qq153471503/article/details/104840279

qmodbus上位机软件下载地址：https://gitee.com/yzhengBTT/qmodbus/releases/V1.0
